CREATE TABLE `good` (
    `id` int NOT NULL AUTO_INCREMENT,
    `good_name` varchar(64) NOT NULL DEFAULT '' COMMENT '商品名称',
    `price` decimal(20,2) NOT NULL DEFAULT 0 COMMENT '商品单价',
    `create_time` int NOT NULL DEFAULT 0 COMMENT '创建时间',
    `update_time` int NOT NULL DEFAULT 0 COMMENT '更新时间',
    `delete_time` int DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`),
    KEY `idx_name` (`id`,`good_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 COMMENT='商品表';

CREATE TABLE `order` (
    `id` int NOT NULL AUTO_INCREMENT,
    `user_id` int NOT NULL DEFAULT 0 COMMENT '用户id',
    `order_no` varchar(64) NOT NULL DEFAULT '' COMMENT '订单编号',
    `total_number` int(11) NOT NULL DEFAULT 0 COMMENT '总数量',
    `total_amount` decimal(20,2) NOT NULL DEFAULT 0 COMMENT '总金额',
    `order_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '订单状态',
    `is_ok` tinyint(1) NOT NULL DEFAULT 0 COMMENT '订单是否生效,成功下单算生效，默认0不生效，1生效',
    `create_time` int NOT NULL DEFAULT 0 COMMENT '创建时间',
    `update_time` int NOT NULL DEFAULT 0 COMMENT '更新时间',
    `delete_time` int DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`),
    KEY `user_id` (`user_id`),
    KEY `order_no` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 COMMENT='订单表';

CREATE TABLE `order_goods` (
    `id` int NOT NULL AUTO_INCREMENT,
    `order_id` int NOT NULL DEFAULT 0 COMMENT '订单id',
    `good_id` int NOT NULL DEFAULT 0 COMMENT '商品id',
    `number` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
    `amount` decimal(20,2) NOT NULL DEFAULT 0 COMMENT '金额',
    `price` decimal(20,2) NOT NULL DEFAULT 0 COMMENT '单价',
    `is_ok` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否生效,成功下单算生效,默认0不生效，1生效',
    `create_time` int NOT NULL DEFAULT 0 COMMENT '创建时间',
    `update_time` int NOT NULL DEFAULT 0 COMMENT '更新时间',
    `delete_time` int DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`),
    KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 COMMENT='订单商品表';

CREATE TABLE `good_stock` (
    `id` int NOT NULL AUTO_INCREMENT,
    `good_id` int NOT NULL DEFAULT 0 COMMENT '商品id',
    `total_number` int(11) NOT NULL DEFAULT 0 COMMENT '剩余数量',
    `create_time` int NOT NULL DEFAULT 0 COMMENT '创建时间',
    `update_time` int NOT NULL DEFAULT 0 COMMENT '更新时间',
    `delete_time` int DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`),
    KEY `good_id` (`good_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 COMMENT='商品库存表';

CREATE TABLE `good_stock_lock` (
    `id` int NOT NULL AUTO_INCREMENT,
    `good_id` int NOT NULL DEFAULT 0 COMMENT '商品id',
    `order_no` varchar(64) NOT NULL DEFAULT '' COMMENT '订单编号',
    `number` int(11) NOT NULL DEFAULT 0 COMMENT '剩余数量',
    `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态，0表示锁定，1表示已扣除，2表示已回滚',
    `create_time` int NOT NULL DEFAULT 0 COMMENT '创建时间',
    `update_time` int NOT NULL DEFAULT 0 COMMENT '更新时间',
    `delete_time` int DEFAULT NULL COMMENT '删除时间',
    PRIMARY KEY (`id`),
    KEY `idx_order_good` (`order_no`,`good_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8mb4 COMMENT='订单商品库存锁定表';

-- 创建商品数据
INSERT INTO `good` (good_name, price, create_time, update_time) values ('小黄瓜',10,1640420585,1640420585);
-- 创建商品库存数据
INSERT INTO `good_stock` (good_id, total_number, create_time, update_time) values (10000,10,1640420585,1640420585)


