<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');
Router::addRoute(['POST'], '/addOrderTry', [\App\Controller\Order::class, 'addOrderTry']);
Router::addRoute(['POST'], '/addOrderConfirm', [\App\Controller\Order::class, 'addOrderConfirm']);
Router::addRoute(['POST'], '/addOrderCancel', [\App\Controller\Order::class, 'addOrderCancel']);

Router::get('/favicon.ico', function () {
    return '';
});
