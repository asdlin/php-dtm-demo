<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

/**
 * @Constants
 */
#[Constants]
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("操作成功")
     */
    public const SUCCESS = 10000;

    /**
     * @Message("系统出小差了 请稍后再试~")
     */
    public const SERVER_ERROR = 500;

    /**
     * @Message("请先登录")
     */
    public const NOT_LOGIN = 10010;

    /**
     * @Message("登录超时")
     */
    public const LOGIN_TIMEOUT = 10011;

    /**
     * @Message("参数错误")
     */
    public const PARAM_ERROR = 10012;

    /**
     * @Message("验证码错误或已过期")
     */
    public const CODE_ERROR = 10013;

    /**
     * @Message("操作过于频繁,请稍后再试")
     */
    public const BUSY_ERROR = 10014;

    /**
     * @Message("上传错误")
     */
    public const UPLOAD_ERROR = 10015;

    /**
     * @Message("出错了")
     */
    public const DEFINE_ERROR = 10016;
}
