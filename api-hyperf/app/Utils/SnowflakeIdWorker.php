<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Utils;

class SnowflakeIdWorker
{
    /** 机器id所占的位数 */
    public const workerIdBits = 10;

    //支持的最大机器id，结果是1023 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数)
    public const maxWorkerId = (-1 ^ (-1 << self::workerIdBits));

    //序列在id中占的位数
    public const sequenceBits = 12;

    //机器ID向左移12位
    public const workerIdShift = self::sequenceBits;

    //时间截向左移22位(10+12)
    public const timestampLeftShift = self::workerIdBits + self::sequenceBits;

    //序列号值的最大值，这里为4095 (0b111111111111=0xfff=4095)
    public const sequenceMask = (-1 ^ (-1 << self::sequenceBits));

    //毫秒内序列(0~4095)：标识符，常驻内存
    public static $sequence = 0;

    //上次生成ID的时间截
    public static $lastTimestamp = -1;

    /** 开始时间截 (2018-01-01) */
    private $startTime;

    //工作机器ID(0~1023)：默认0
    private $workerId = 0;

    /*
     * 构造函数：设置当前机器id
     * SnowflakeIdWorker constructor.
     * @param $workerId
     */
    public function __construct($workerId)
    {
        $this->startTime = strtotime('2021-12-25');
        //转换类型
        $workerId = (int) $workerId;

        //判断参数合法性
        if ($workerId < 0 || $workerId > self::maxWorkerId) {
            exit('error...');
        }

        //设置当前机器id
        $this->workerId = $workerId;
    }

    public function nextId()
    {
        //获取当前毫秒时间戳
        $timestamp = $this->timeGen();
        //获取上一次生成id时的毫秒时间戳
        $lastTimestamp = self::$lastTimestamp;

        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if ($timestamp < $lastTimestamp) {
            exit('error...');
        }

        //如果是同一毫秒内生成的，则进行毫秒序列化
        if ($timestamp == $lastTimestamp) {
            //获取当前序列号值
            self::$sequence = (self::$sequence + 1) & self::sequenceMask;
            //毫秒序列化值溢出（就是超过了4095）
            if (self::$sequence == 0) {
                //阻塞到下一秒，获得新的时间戳
                $timestamp = $this->tilNextMillis($lastTimestamp);
            }
        }
        //如果不是同一毫秒，那么重置毫秒序列化值
        else {
            self::$sequence = 0;
        }

        //重置上一次生成的时间戳
        self::$lastTimestamp = $timestamp;

        //移位并通过或运算拼到一起组成64位的ID
        return
            //时间戳左移 22 位
            (($timestamp - $this->startTime) << self::timestampLeftShift) |
            //机器id左移 12 位
            ($this->workerId << self::workerIdShift) |
            //或运算序列号值
            self::$sequence;
    }

    /*
     * 阻塞到下一个毫秒，直到获得新的时间戳
     * @param $lastTimestamp 上次生成ID的时间截
     * @return float 当前毫秒时间戳
     */
    private function tilNextMillis($lastTimestamp)
    {
        //重新获取当前时间戳
        $timestamp = $this->timeGen();
        //如果等于上一次获取的时间戳，仍然重新获取
        while ($timestamp <= $lastTimestamp) {
            $timestamp = $this->timeGen();
        }
        //返回新的时间戳
        return $timestamp;
    }

    /*
     * 返回当前毫秒时间戳
     * @return float
     */
    private function timeGen()
    {
        return (float) sprintf('%.0f', microtime(true) * 1000);
    }
}
