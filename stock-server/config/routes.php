<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');

Router::addRoute(['POST'], '/decGoodStockTry', [\App\Controller\Stock::class, 'decGoodStockTry']);
Router::addRoute(['POST'], '/decGoodStockConfirm', [\App\Controller\Stock::class, 'decGoodStockConfirm']);
Router::addRoute(['POST'], '/decGoodStockCancel', [\App\Controller\Stock::class, 'decGoodStockCancel']);

Router::get('/favicon.ico', function () {
    return '';
});
